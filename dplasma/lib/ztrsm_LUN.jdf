extern "C" %{
/*
 *  Copyright (c) 2010-2013
 *
 *  The University of Tennessee and The University
 *  of Tennessee Research Foundation.  All rights
 *  reserved.
 *
 * @precisions normal z -> s d c
 *
 */
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

%}

side      [type = "PLASMA_enum"]
uplo      [type = "PLASMA_enum"]
trans     [type = "PLASMA_enum"]
diag      [type = "PLASMA_enum"]
alpha     [type = "parsec_complex64_t"]
descA     [type = "const tiled_matrix_desc_t*"]
descB     [type = "tiled_matrix_desc_t*"]


ztrsm(k, n)
  /* Execution Space */
  k = 0 .. (descB->mt-1)
  n = 0 .. (descB->nt-1)

  : descB((descB->mt-1)-k,n)

  READ  A <- A ztrsm_in_A0(k)

  RW    B <- (0==k) ? descB((descB->mt-1)-k,n)
          <- (k>=1) ? E zgemm(k-1, k, n)
          -> descB((descB->mt-1)-k,n)
          -> (descB->mt>=(2+k)) ? D zgemm(k, (k+1)..(descB->mt-1), n)

BODY
{
    int tempkm = ((k)==(0))          ? (descB->m-((descB->mt-1)*descB->mb)) : descB->mb;
    int tempnn = ((n)==(descB->nt-1)) ? (descB->n-(n*descB->nb)) : descB->nb;
    parsec_complex64_t lalpha = ((k)==(0)) ? (alpha) : (parsec_complex64_t)(1.0);
    int lda = BLKLDD( descA, (descB->mt-1)-k );
    int ldb = BLKLDD( descB, (descB->mt-1)-k );

#if !defined(PARSEC_DRY_RUN)
        CORE_ztrsm(side, uplo, trans, diag,
                   tempkm, tempnn, lalpha,
                   A /* descA((descB->mt-1)-k,(descB->mt-1)-k) */, lda,
                   B /* descB((descB->mt-1)-k,n)              */, ldb );
#endif /* !defined(PARSEC_DRY_RUN) */

    printlog("CORE_ztrsm(%d, %d)\n"
             "\t(side, uplo, trans, diag, tempkm, tempnn, lalpha, descA(%d,%d)[%p], lda, descB(%d,%d)[%p], ldb)\n",
             k, n, (descB->mt-1)-k, (descB->mt-1)-k, A, (descB->mt-1)-k, n, B);
}
END

/*
 * Pseudo-task
 */
ztrsm_in_A0(k) [profile = off]
  k = 0 .. (descB->mt-1)

  : descA((descB->mt-1)-k,(descB->mt-1)-k)

  RW A <- descA((descB->mt-1)-k,(descB->mt-1)-k)
       -> A ztrsm(k,0..(descB->nt-1))
BODY
{
    /* nothing */
}
END


zgemm(k,m,n)
  /* Execution space */
  k = 0     .. (descB->mt-2)
  m = (k+1) .. (descB->mt-1)
  n = 0     .. (descB->nt-1)

  : descB((descB->mt-1)-m,n)

  READ  C <- C zgemm_in_A0(k,m)

  READ  D <- B ztrsm(k, n)
  RW    E <- (k>=1) ? E zgemm(k-1, m, n)
          <- (0==k) ? descB((descB->mt-1)-m,n)
          -> ((1+k)==m) ? B ztrsm(m, n)
          -> (m>=(k+2)) ? E zgemm(k+1, m, n)

BODY
{
    int tempnn = ((n)==(descB->nt-1)) ? (descB->n-(n*descB->nb)) : descB->nb;
    int tempkm = ((k)==(0)) ? (descB->m-((descB->mt-1)*descB->mb)) : descB->mb;
    parsec_complex64_t lalpha = ((k)==(0)) ? (alpha) : (parsec_complex64_t)(1.0);
    int ldam = BLKLDD( descB, (descA->mt-1)-m );
    int ldbm = BLKLDD( descB, (descB->mt-1)-m );
    int ldb  = BLKLDD( descB, (descB->mt-1)-k );

#if !defined(PARSEC_DRY_RUN)
        CORE_zgemm(PlasmaNoTrans, PlasmaNoTrans, descB->mb,
                   tempnn, tempkm,
                   -1.0,   C /* descA((descB->mt-1)-m,(descB->mt-1)-k) */, ldam,
                           D /* descB((descB->mt-1)-k,n) */,              ldb,
                   lalpha, E /* descB((descB->mt-1)-m,n) */,              ldbm );
#endif /* !defined(PARSEC_DRY_RUN) */
    printlog("CORE_zgemm(%d, %d, %d)\n"
             "\t(PlasmaNoTrans, PlasmaNoTrans, descB->mb, tempnn, tempkm, mzone, descA(%d,%d)[%p], descA->mb, descB(%d,%d)[%p], ldb, lalpha, descB(%d,%d)[%p], descB->mb)\n",
             k, m, n, (descB->mt-1)-m, (descB->mt-1)-k, C, (descB->mt-1)-k, n, D, (descB->mt-1)-m, n, E);
}
END

/*
 * Pseudo-task
 */
zgemm_in_A0(k,m) [profile = off]
  k = 0     .. (descB->mt-2)
  m = (k+1) .. (descB->mt-1)

  : descA((descB->mt-1)-m,(descB->mt-1)-k)

  RW C <- descA((descB->mt-1)-m,(descB->mt-1)-k)
       -> C zgemm(k,m,0..(descB->nt-1))
BODY
{
    /* nothing */
}
END

