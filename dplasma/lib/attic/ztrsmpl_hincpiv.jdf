extern "C" %{
/*
 *  Copyright (c) 2010-2013
 *
 *  The University of Tennessee and The University
 *  of Tennessee Research Foundation.  All rights
 *  reserved.
 *
 * @precisions normal z -> s d c
 *
 */
#define PRECISION_z

#include "parsec.h"
#include <math.h>
#include <core_blas.h>
#include <core_blas.h>

#include "data_distribution.h"
#include "data_dist/matrix/precision.h"
#include "data_dist/matrix/matrix.h"
#include "dplasma/lib/memory_pool.h"
#include "dplasma/lib/dplasmajdf.h"

%}

A      [type = "parsec_ddesc_t *"]
descA  [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)A)"]
IPIV   [type = "parsec_ddesc_t *" aligned=A]
B      [type = "parsec_ddesc_t *"]
descB  [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)B)"]
LT     [type = "parsec_ddesc_t *" aligned=A]
descLT [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)LT)"]
qrtree [type = "dplasma_qrtree_t"]
ib     [type = "int"]
p_work [type = "parsec_memory_pool_t *" size = "(sizeof(PLASMA_Complex64_t)*ib*(descLT.nb))"]
p_tau  [type = "parsec_memory_pool_t *" size = "(sizeof(PLASMA_Complex64_t)   *(descLT.nb))"]
INFO   [type = "int*"]

minMN  [type = "int" hidden=on default="( (descA.mt < descA.nt) ? descA.mt : descA.nt )" ]


/**
 * zgessm()
 *
 * (see zgetrf() for details on definition space)
 */
zgessm(k, i, n)
  /* Execution space */
  k = 0 .. minMN-1
  i = 0 .. %{ return qrtree.getnbgeqrf( &qrtree, k ) - 1; %}
  n = 0 .. descB.nt-1
  m     = %{ return qrtree.getm(    &qrtree, k, i); %}
  nextm = %{ return qrtree.nextpiv( &qrtree, k, m, descA.mt); %}

  SIMCOST 6

  : B(m, n)

  READ  A <- A zgessm_in(k,i)                                                    [type = LOWER_TILE]
  READ  P <- P zgessm_in(k,i)                                                    [type = PIVOT]

  RW    C <- ( 0 == k ) ? B(m, n)
          <- ( k >  0 ) ? A2 zttmqr(k-1, m, n)
          -> ( k == descA.mt-1 ) ? B(m, n)
          -> ( (k < descA.mt-1) & (nextm != descA.mt) ) ? A1 zttmqr(k, nextm, n)
          -> ( (k < descA.mt-1) & (nextm == descA.mt) ) ? A2 zttmqr(k, m,     n)

BODY
{
    int tempknA = (k == (descA.nt-1)) ? (descA.n - k * descA.nb) : descA.nb;
    int tempmmB = (m == (descB.mt-1)) ? (descB.m - m * descB.mb) : descB.mb;
    int tempnnB = (n == (descB.nt-1)) ? (descB.n - n * descB.nb) : descB.nb;
    int tempk   = dplasma_imin( tempknA, tempmmB );
    int ldam    = BLKLDD( descA, m );
    int ldbm    = BLKLDD( descB, m );

    printlog("\nthread %d VP %d   zgessm( k=%d, i=%d, n=%d, m=%d, nextm=%d)\n"
             "\t( m=%d, n=%d, k=%d, ib, IPIV(%d,%d)[%p], \n"
             "\t  A(%d,%d)[%p], ldam = %d, A(%d,%d)[%p], ldam = %d)\n",
             context->th_id, context->virtual_process->vp_id, k, i, n, m, nextm,
             tempmmB, tempnnB, tempk, m, k, P, m, k, A, ldam, m, n, C, ldbm);

#if !defined(PARSEC_DRY_RUN)
           CORE_zgessm(tempmmB, tempnnB, tempk, ib,
                       P /* IPIV(m,k) */,
                       A /* A(m,k)    */, ldam,
                       C /* B(m,n)    */, ldbm );
#endif /* !defined(PARSEC_DRY_RUN) */
}
END

zgessm_in(k, i) [profile = off]
  /* Execution space */
  k = 0 .. minMN-1
  i = 0 .. %{ return qrtree.getnbgeqrf( &qrtree, k ) - 1; %}
  m     = %{ return qrtree.getm(    &qrtree, k, i); %}

  : A(m,k)

  READ A <- A(m,k)                                                    [type = LOWER_TILE]
         -> A zgessm(k, i, 0..descB.nt-1)                             [type = LOWER_TILE]
  READ P <- IPIV(m,k)                                                 [type = PIVOT]
         -> P zgessm(k, i, 0..descB.nt-1)                             [type = PIVOT]

BODY
{
    /* Nothing */
}
END


/**
 * zttmqr()
 *
 * See also zttqrt()
 * type1 defines the operations to perfom at next step k+1 on the row m
 *   if type1 == 0, it will be a TS so the tile goes to a TTQRT/TTMQR operation
 *   if type1 != 0, it will be a TT so the tile goes to a GEQRT/UNMQR operation
 * im1 is the index of the killer m at the next step k+1 if its type is !0, descA.mt otherwise
 *
 */
zttmqr(k, m, n)
  /* Execution space */
  k = 0   .. minMN-1
  m = k+1 .. descA.mt-1
  n = 0   .. descB.nt-1
  p =     %{ return qrtree.currpiv( &qrtree, k,   m);    %}
  nextp = %{ return qrtree.nextpiv( &qrtree, k,   p, m); %}
  prevp = %{ return qrtree.prevpiv( &qrtree, k,   p, m); %}
  prevm = %{ return qrtree.prevpiv( &qrtree, k,   m, m); %}
  type  = %{ return qrtree.gettype( &qrtree, k,   m );   %}
  type1 = %{ return qrtree.gettype( &qrtree, k+1, m );   %}
  ip    = %{ return qrtree.geti(    &qrtree, k,   p );   %}
  im    = %{ return qrtree.geti(    &qrtree, k,   m );   %}
  im1   = %{ return qrtree.geti(    &qrtree, k+1, m );   %}

  SIMCOST %{ return type == DPLASMA_QR_KILLED_BY_TS ? 12 : 6; %}

  : B(m, n)

  RW   A1 <- (   prevp == descA.mt ) ? C  zgessm( k, ip, n ) : A1 zttmqr(k, prevp, n )
          -> (   nextp != descA.mt ) ? A1 zttmqr( k, nextp, n)
          -> ( ( nextp == descA.mt ) && ( p == k ) ) ? A  zttmqr_out_A1(p, n)
          -> ( ( nextp == descA.mt ) && ( p != k ) ) ? A2 zttmqr( k, p, n )

  RW   A2 <- ( (type == 0) && (k     == 0        ) ) ? B(m, n)
          <- ( (type == 0) && (k     != 0        ) ) ? A2 zttmqr(k-1, m, n )
          <- ( (type != 0) && (prevm == descA.mt ) ) ? C  zgessm(k, im, n)
          <- ( (type != 0) && (prevm != descA.mt ) ) ? A1 zttmqr(k, prevm, n )
          -> (type1 != 0) ? C  zgessm( k+1, im1, n )
          -> (type1 == 0) ? A2 zttmqr( k+1, m,   n )

  READ  V <- (type == 0) ? A zttmqr_in(k, m)
          <- (type != 0) ? A zttmqr_in(k, m)     [type = UPPER_TILE]

  READ  L <- L zttmqr_in(k,m)                    [type = SMALL_L]

  READ  P <- (type == 0 ) ? P zttmqr_in(k,m)     [type = PIVOT]
          <- (type != 0 ) ? B(m,n)               /*never used*/

BODY
{
    int tempnn = (n == (descB.nt-1)) ? (descB.n - n * descB.nb) : descB.nb;
    int tempmm = (m == (descB.mt-1)) ? (descB.m - m * descB.mb) : descB.mb;
    int tempkn = (k == (descA.nt-1)) ? (descA.n - k * descA.nb) : descA.nb;
    int ldbp = BLKLDD( descB, p );
    int ldbm = BLKLDD( descB, m );
    int ldam = BLKLDD( descA, m );

    printlog("\nthread %d VP %d zttmqr( k=%d, m=%d, n=%d, p=%d, nextp=%d, prevp=%d, prevm=%d, type=%d, type1=%d, ip=%d, im=%d, im1=%d)\n"
             "\t(M1=%d, N1=%d, M2=%d, N2=%d, K=%d, ib=%d, \n"
             "\t B(%d,%d)[%p], %d, B(%d,%d)[%p], %d,\n"
             "\t LT(%d,%d)[%p], descLT.mb=%d, A(%d,%d)[%p], %d)\n",
             context->th_id, context->virtual_process->vp_id, k, m, n, p, nextp, prevp, prevm, type, type1, ip, im, im1,
             descB.mb, tempnn, tempmm, tempnn, tempkn, ib,
             p, n, A1, ldbp, m, n, A2, ldbm,
             m, k, L, descLT.mb, m, k, V, ldam);

#if !defined(PARSEC_DRY_RUN)

        if ( type == DPLASMA_QR_KILLED_BY_TS ) {
            CORE_zssssm(
                descB.mb, tempnn, tempmm, tempnn, tempkn, ib,
                A1 /* B(p, n)  */, ldbp,
                A2 /* B(m, n)  */, ldbm,
                L  /* LT(m, k) */, descLT.mb,
                V  /* A(m, k)  */, ldam,
                P  /* IPIV(m,k)*/ );
        } else {
            void *p_elem_A = parsec_private_memory_pop( p_work );

            CORE_zttmqr(
                PlasmaLeft, PlasmaConjTrans,
                descB.mb, tempnn, tempmm, tempnn, tempkn, ib,
                A1 /* B(p, n)  */, ldbp,
                A2 /* B(m, n)  */, ldbm,
                V  /* A(m, k)  */, ldam,
                L  /* LT(m, k) */, descLT.mb,
                p_elem_A, ib );

            parsec_private_memory_push( p_work, p_elem_A );
        }

#endif /* !defined(PARSEC_DRY_RUN) */
}
END

zttmqr_out_A1(k, n) [profile = off]
  k = 0 .. minMN-2
  n = 0 .. descB.nt-1
  prevp = %{ return qrtree.prevpiv( &qrtree, k, k, k); %}

  : B(k, n)

  RW A <- A1 zttmqr( k, prevp, n )
       -> B(k, n)
BODY
{
    /* Nothing */
}
END

zttmqr_in(k, m) [profile = off]
  /* Execution space */
  k = 0   .. minMN-1
  m = k+1 .. descA.mt-1
  type = %{ return qrtree.gettype( &qrtree, k, m );   %}

  : A(m,k)

  READ A <- A(m,k)
         -> (type == 0) ? V zttmqr(k, m, 0..descB.nt-1)
         -> (type != 0) ? V zttmqr(k, m, 0..descB.nt-1)                [type = UPPER_TILE]

  READ P <- IPIV(m,k)                                                  [type = PIVOT]
         -> (type == 0) ? P zttmqr(k, m, 0..descB.nt-1)                [type = PIVOT]

  READ L <- LT(m,k)                                                    [type = SMALL_L]
         -> L zttmqr(k,m,0..descB.nt-1)                                [type = SMALL_L]

BODY
{
    /* Nothing */
}
END
